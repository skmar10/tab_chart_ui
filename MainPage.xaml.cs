﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Syncfusion.UI.Xaml.Gauges;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace TabControl
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {

        private DispatcherTimer timer = new DispatcherTimer();
        SfCircularGauge circularGauge = new SfCircularGauge();
        public Color uwp_Color;
        


        //private MainPageViewModel vm; 

        public int percentage_differnce = 85;
        public int feature_1_val = 65;
        public int feature_2_val = 55;
        public int feature_3_val = 35;
        public int feature_4_val = 95;
        public int feature_5_val = 15;



        public MainPage()
        {
            this.InitializeComponent();
            showRadial();
            setLinearGaugeValue();


        }

        public void showRadial()
        {
            //timer.Interval = TimeSpan.FromSeconds(2); 
            timer.Start();
            int i = 0;

            timer.Tick += (object sender, object e) =>
            {

                if (i == percentage_differnce + 1)
                {
                    timer.Stop();
                    if (percentage_differnce >= 80)
                    {
                        uwp_Color = Convert_Hex_to_UIColor("#e74c3c");
                        radialgaugecolor.Color = uwp_Color;
                    }
                }
                else
                {
                    myGrapg.Value = i;
                    i++;
                }
            };
        }

        public void setLinearGaugeValue()
        {
            pointer1.Value = feature_1_val;
            pointer2.Value = feature_2_val;
            pointer3.Value = feature_3_val;
            pointer4.Value = feature_4_val;
            pointer5.Value = feature_5_val;

        }

        private void Pivot_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            for (int i = 0; i < Pivot.Items.Count; i++)
            {
                if (i == Pivot.SelectedIndex)
                {
                    PivotItem selectedPivotItem = Pivot.SelectedItem as PivotItem;
                    (selectedPivotItem.Header as TextBlock).Foreground = new SolidColorBrush(Colors.Black);
                }
                else
                {
                    PivotItem unselectedPivotItem = Pivot.Items[i] as PivotItem;
                    (unselectedPivotItem.Header as TextBlock).Foreground = new SolidColorBrush(Colors.LightBlue);
                }
            }
        }


        public static Windows.UI.Color Convert_Hex_to_UIColor(string string_HexColor)
        {
            //----------------<  Convert_Hex_to_UIColor() >----------------
            //original source: http://snipplr.com/view/13358/
            //Remove # if present
            if (string_HexColor.IndexOf('#') != -1)
                string_HexColor = string_HexColor.Replace("#", "");

            //< variables >
            int alpha = 255;
            int red = 0;
            int green = 0;
            int blue = 0;
            //</ variables >

            //----< Convert String to int >---
            if (string_HexColor.Length == 6)
            {
                //--< RRGGBB_to_int >--
                //*like: #RRGGBB
                red = int.Parse(string_HexColor.Substring(0, 2), System.Globalization.NumberStyles.AllowHexSpecifier);
                green = int.Parse(string_HexColor.Substring(2, 2), System.Globalization.NumberStyles.AllowHexSpecifier);
                blue = int.Parse(string_HexColor.Substring(4, 2), System.Globalization.NumberStyles.AllowHexSpecifier);
                //--</ RRGGBB_to_int >--
            }
            else if (string_HexColor.Length == 3)
            {
                //--< RGB_to_int >--
                //not really necessary
                //*like #RGB
                red = int.Parse(string_HexColor[0].ToString() + string_HexColor[0].ToString(), System.Globalization.NumberStyles.AllowHexSpecifier);
                green = int.Parse(string_HexColor[1].ToString() + string_HexColor[1].ToString(), System.Globalization.NumberStyles.AllowHexSpecifier);
                blue = int.Parse(string_HexColor[2].ToString() + string_HexColor[2].ToString(), System.Globalization.NumberStyles.AllowHexSpecifier);
                //--</ RGB_to_int >--
            }
            //----</ Convert String to int >---

            //< Return >
            return Windows.UI.Color.FromArgb(Convert.ToByte(alpha), Convert.ToByte(red), Convert.ToByte(green), Convert.ToByte(blue));
            //</ Return >
            //----------------</  Convert_Hex_to_UIColor() >----------------
        }

    }
 }
